import React from 'react';
import { Layout, Collapse} from 'antd';
const { Content } = Layout ;

const Panel = Collapse.Panel;

const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

const customPanelStyle = {
  background: '#f7f7f7',
  borderRadius: 4,
  marginBottom: 24,
  border: 0,
  overflow: 'hidden',
};
const htm = (
    <div>
        <p>Sardi Mehta | 
        <span>Benu devi | </span>
        <span>High Risk | </span>
        </p>
        
    </div>
)

export class CollapsePatientDetails extends React.Component {
    render(){
        return(
            <Content style={{ margin: '24px 16px', padding: 24, background: '#fff' }} >
                <Collapse bordered={false} defaultActiveKey={['1']} >
                    <Panel header={htm} key="1" style={customPanelStyle}>
                        <p>{text}</p>
                    </Panel>
                    <Panel header="This is panel header 2" key="2" style={customPanelStyle}>
                        <p>{text}</p>
                    </Panel>
                    <Panel header="This is panel header 3" key="3" style={customPanelStyle}>
                        <p>{text}</p>
                    </Panel>
                </Collapse>
            </Content>
        )
    }
}
