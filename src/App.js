import React, { Component } from 'react';
import './App.css';
import { DashboardLayout } from './layout/dashboard';
import { LoginView } from './views/login/login';
import { Router, Route , Switch } from 'react-router-dom';
import {historyHGB} from './helper/history';
import { DashboardView } from './views/dashboard/dashboard';

class App extends Component {
  render() {
    return (
      <Router history={historyHGB}>
        <Switch>
          <Route exact path='/' component = {LoginView} />
          <Route path='/db' component = {DashboardView} />
          <Route component = {DashboardLayout} />
        </Switch>
      </Router>
    );
  }
}

export default App;
