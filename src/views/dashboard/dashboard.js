import React from 'react';
import { DashboardLayout } from '../../layout/dashboard';
import { CollapsePatientDetails } from '../../components/collapse/collapse';
import { DashboardTable } from '../../components/table/table';

export class DashboardView extends React.Component {
    render(){
        return(
            <DashboardLayout>
                <CollapsePatientDetails/>
                <DashboardTable/>
            </DashboardLayout>
        )
    }
}